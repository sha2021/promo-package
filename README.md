Hi,

This is a promo package for SHA2021. Pull requrests welcome.

# General Guidance
- Use open resources, or ask explicit permission for public usage.
- Add the highest resolution images.
- Use vector over rasterized where possible.

## Images
- Blur faces (and other uniquely recognizable items) when there is no explicit consent given for publication.
- Always attribute IN the filename. For example: "example CC BY stitch.jpg".
